from .event import Event2


class DestroyEvent(Event2):
    NAME = "destroy"

    def perform(self):
        self.object.move_to(None)
        self.inform("destroy")
