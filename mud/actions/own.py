from .action import Action2
from mud.events import RemonterEvent, AbaisserEvent

class RemonterAction(Action2):
    EVENT = RemonterEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "remonter"

class AbaisserAction(Action2):
    EVENT = AbaisserEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "abaisser"
