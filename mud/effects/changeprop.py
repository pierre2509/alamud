# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================
from .effect import Effect2, Effect1
from mud.events import ChangePropEvent, ChangeOwnPropEvent

class ChangePropEffect(Effect2):
    EVENT = ChangePropEvent

    def make_event(self):
        return self.EVENT(self.actor, self.object, self.yaml["modifs"])

class ChangeOwnPropEffect(Effect1):
    EVENT = ChangeOwnPropEvent

    def make_event(self):
        return self.EVENT(self.actor, self.yaml["modifs"])