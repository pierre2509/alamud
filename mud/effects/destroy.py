from .effect import Effect2
from mud.events import DestroyEvent

class DestroyEffect(Effect2):
    EVENT = DestroyEvent
